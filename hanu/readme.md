## Focus

This directory focuses on biological data that, by extension, analyses trends that affect coral reef health in the Great Barrier Reef.

## Data Source

The data is collected from the [AODN Portal](https://portal.aodn.org.au/) and bound to the Great Barrier Region. The range of the data is from 2000-2022.

## Data Cleaning

The data was cleaned two ways:

- Python via pandas, to clean columns, values and dangling text.
- Excel spreadsheets. CSV uploads and then the use of excel formulae.

## Data Analysis and Visualization

The data was summarized and analysed using [Exploratory](https://www.exploratory.io/), which is a tool built on R that helps you Data Wrangle, Analyse, Visualize, etc.

## Coral Bleaching References

[Australia Coral Bleaching Report](https://www.aims.gov.au/research-topics/environmental-issues/coral-bleaching/coral-bleaching-events)
[Great Barrier Reef Foundation Report](https://www.barrierreef.org/the-reef/threats/coral-bleaching)
