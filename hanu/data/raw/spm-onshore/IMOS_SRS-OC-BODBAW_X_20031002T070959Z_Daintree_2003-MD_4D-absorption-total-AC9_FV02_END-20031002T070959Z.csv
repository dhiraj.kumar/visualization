GLOBAL ATTRIBUTES,,,,,,,,,,,,,,,,,,,,,,,
title,,,,,,,,,,,,,,,,,,,,,,,
project,Integrated Marine Observing System (IMOS),,,,,,,,,,,,,,,,,,,,,,
original_project_name,MERIS validation,,,,,,,,,,,,,,,,,,,,,,
institution,CSIRO Land and Water,,,,,,,,,,,,,,,,,,,,,,
abstract,This record contains in situ water optical property data collected in the Mossman-Daintree (Great Barrier Reef| Australia) on the RV Foundation One between 29 Sept-03 Oct 2003. A total of 12 stations were sampled during the field campaign MD03. The data can be used for Ocean Colour sensor validation| optical water quality monitoring and modelling. CLW deployed the optical instruments TriOS RAMSES (hyperspectral irradiance)| WETLabs AC-9 (absorption and attenuation coefficicents)| HOBILabs Hydroscat-6 (backscattering coefficient) during this cruise.This data was part of the dataset described in Oubelkheir et al. 2004| Bio-optical characterisation of Australian ocean| coastal and estuarine waters In SPIE| Ocean Optics XVII| Fremantle (Aus.) and Blondeau-Patissier et al. 2009| JGR| VOL. 114| C05003| doi:10.1029/2008JC005039. Funding for this study was provided by the Queensland Environmental Protection Agency| CSIRO Land and Water| CSIRO Marine and Atmospheric Research| and Coastal Catchments Initiative We also thank the captains and crews of the RV Foundation One.,,,,,,,,,,,,,,,,,,,,,,
conventions,IMOS biological data format (v1.0),,,,,,,,,,,,,,,,,,,,,,
date_created,,,,,,,,,,,,,,,,,,,,,,,
date_modified,,,,,,,,,,,,,,,,,,,,,,,
history,Data has been quality controlled by in-house procedures. AC-9 data were corrected for temperature using CTD profiles following Pegau et al.| 1997| Applied Optics| 36(24)| pp.6035-6046. The absorption coefficient was corrected for scattering using the proportional method described in Zaneveld et al.| 1992| Ocean Optics XI| vol.1750| pp.187-199| San Diego| CA,,,,,,,,,,,,,,,,,,,,,,
comment,The AC-9 measures the spectral absorption| A| and spectral attenuation| C| over nine wavelengths via a dual-path measurement. An AC-9 is used to measure the total absorption and attenuation of water (the signal includes both dissolved and particulate absorption). The AC-9 measurements were referenced to optically clean water| and resulting values thus represent the absorption and attenuation of the sample water minus the calibration water values. If the ocean water is very clear| it may be assumed that the measured absorption and attenuation represent everything in the water except pure water itself. To derive total absorption and total attenuation| it was therefore necessary to add in the absorption and attenuation| respectively| of pure water. Currently| the most accurate pure water absorption values are considered to be those presented by Pope and Fry (1997| Applied Optics| 36(33)| pp.8710-8723).,,,,,,,,,,,,,,,,,,,,,,
source,Total Absorption coefficients determined using an AC-9 (www.wetlabs.com),,,,,,,,,,,,,,,,,,,,,,
keywords,Oceans>Ocean Optics > Absorption ;Oceans>Ocean Optics>Ocean Colour;,,,,,,,,,,,,,,,,,,,,,,
cruise_id,Daintree_2003,,,,,,,,,,,,,,,,,,,,,,
vessel_name,RV Foundation One,,,,,,,,,,,,,,,,,,,,,,
platform_code,RVFO,,,,,,,,,,,,,,,,,,,,,,
references,https://marlin.csiro.au/geonetwork/srv/eng/catalog.search#/metadata/c0af5903-2d8a-0b28-e044-00144f7bc0f4,,,,,,,,,,,,,,,,,,,,,,
naming_authority,CSIRO Land and Water (CLW),,,,,,,,,,,,,,,,,,,,,,
cdm_data_type,profile,,,,,,,,,,,,,,,,,,,,,,
geospatial_lat_min,-16.43,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_lat_max,-16.43,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_lon_min,145.5,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_lon_max,145.5,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_vertical_min,0,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_vertical_max,5.2,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
local_time_zone,10,AEST (UTC +10),,,,,,,,,,,,,,,,,,,,,
data_centre,eMarine Information Infrastructure (eMII),,,,,,,,,,,,,,,,,,,,,,
data_centre_email,info@emii.org.au,,,,,,,,,,,,,,,,,,,,,,
author,"Brando, Vittorio",,,,,,,,,,,,,,,,,,,,,,
author_email,vittorio.brando@csiro.au,,,,,,,,,,,,,,,,,,,,,,
principal_investigator,"Steven, Andy",,,,,,,,,,,,,,,,,,,,,,
principal_investigator_email,Andy.Steven@csiro.au,,,,,,,,,,,,,,,,,,,,,,
publisher,Australian Ocean Data Network,,,,,,,,,,,,,,,,,,,,,,
publisher_email,info@aodn.org.au,,,,,,,,,,,,,,,,,,,,,,
institution,IMOS Satellite Remote Sensing - Ocean Colour Sub-facility - Bio-Optical Database of Australian Waters,,,,,,,,,,,,,,,,,,,,,,
institution_principal_investigator,"Schroeder, Thomas",,,,,,,,,,,,,,,,,,,,,,
institution_principal_investigator_email,Thomas.Schroeder@csiro.au,,,,,,,,,,,,,,,,,,,,,,
citation,"To cite this data set in a publication, use the format ""IMOS [year-of-data-downloaded], [Title], [data-access-url], accessed [date-of-access]""",,,,,,,,,,,,,,,,,,,,,,
acknowledgement,Data was sourced from the Commonwealth Scientific and Industrial Research Organisation (CSIRO),,,,,,,,,,,,,,,,,,,,,,
distribution_statement,Any users of IMOS data are required to clearly acknowledge the source of the material. Data, products and services from IMOS are provided as is without any warranty as to fitness for a particular purpose.,,,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
TABLE ROWS,,,,,,,,,,,,,,,,,,,,,,,
Row name,CF standard_name,IMOS long_name,Units,Fill value,Comments,,,,,,,,,,,,,,,,,,
Wavelength,radiation_wavelength,Wavelength of measuement,nm,-999,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
TABLE COLUMNS,,,,,,,,,,,,,,,,,,,,,,,
Column name,CF standard_name,IMOS long_name,Units,Fill value,Comments,,,,,,,,,,,,,,,,,,
Time,time,analysis_time,YYYY-MM-DDThh:mm:ssZ,,UTC date and time,,,,,,,,,,,,,,,,,,
Station_Code,,,,,National Reference Station code (6 letters),,,,,,,,,,,,,,,,,,
Latitude,latitude,latitude,degrees_north,-999,North positive,,,,,,,,,,,,,,,,,,
Longitude,longitude,longitude,degrees_east,-999,East positive,,,,,,,,,,,,,,,,,,
Depth,depth,depth,m,-999,positive down,,,,,,,,,,,,,,,,,,
ac9_a_corr,,corrected_volume_total_absorption_coefficient_in_sea_water,m-1,-999,,,,,,,,,,,,,,,,,,,
ac9_c_corr,,corrected_volume_total_attenuation_coefficient_in_sea_water,m-1,-999,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
DATA,,,,,,,,,,,,,,,,,,,,,,,
,Time,Station_Code,Latitude,Longitude,Depth,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr
Wavelength,,,,,,412,440,488,510,532,555,650,676,715,412,440,488,510,532,555,650,676,715
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,1.4,1.0088,0.6867,0.3834,0.2891,0.2081,0.1507,0.0534,0.0783,0,3.1865,2.7817,2.3645,2.2199,2.0786,1.9713,1.6453,1.5977,1.5128
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,1.6,0.9996,0.6832,0.3811,0.288,0.2089,0.151,0.0514,0.0758,0,3.1674,2.7691,2.3555,2.2157,2.0737,1.9641,1.6407,1.5936,1.5114
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,1.8,0.9778,0.6664,0.3731,0.2787,0.201,0.1416,0.05,0.0705,0,3.11,2.7302,2.3283,2.1976,2.0593,1.9537,1.632,1.5846,1.4967
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,2,0.9688,0.6613,0.3721,0.2856,0.2041,0.1504,0.0547,0.0761,0,3.0512,2.6719,2.288,2.1506,2.0144,1.911,1.6059,1.5614,1.4853
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,2.2,0.9514,0.6519,0.3615,0.2741,0.2014,0.1418,0.0575,0.073,0,2.9781,2.6142,2.2312,2.0951,1.9656,1.8652,1.5616,1.5203,1.4477
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,2.4,0.9281,0.6343,0.3565,0.2674,0.1958,0.1384,0.0561,0.066,0,2.8955,2.5287,2.1552,2.0424,1.9137,1.8183,1.5298,1.4816,1.3992
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,2.6,0.9216,0.6242,0.3515,0.2635,0.1969,0.1371,0.053,0.0587,0,2.9274,2.5749,2.2039,2.0784,1.9492,1.864,1.567,1.5234,1.4368
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,2.8,0.9367,0.6373,0.3592,0.2719,0.2036,0.1384,0.05,0.0555,0,3.1053,2.7536,2.3843,2.2579,2.1325,2.0342,1.7331,1.6872,1.6101
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,3,0.9339,0.634,0.3565,0.2656,0.1941,0.14,0.0497,0.0567,0,3.0908,2.7363,2.3788,2.2443,2.1145,2.0121,1.7189,1.6801,1.6002
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,3.2,0.8649,0.5932,0.3378,0.2516,0.1863,0.1325,0.0432,0.05,0,2.9231,2.6085,2.27,2.1517,2.0326,1.9368,1.6626,1.6222,1.5447
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,3.4,0.8653,0.595,0.3349,0.2512,0.1859,0.1295,0.0438,0.0518,0,2.9381,2.621,2.2831,2.1601,2.0382,1.9504,1.6686,1.6295,1.5589
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,3.6,0.8351,0.5733,0.3241,0.2468,0.1841,0.1273,0.044,0.0526,0,2.8322,2.5267,2.201,2.0936,1.9665,1.8927,1.634,1.5833,1.5156
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,3.8,0.8166,0.5603,0.3166,0.2422,0.1703,0.1169,0.0424,0.0543,0,2.8112,2.5054,2.2026,2.0905,1.985,1.9052,1.6436,1.6001,1.5384
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,4,0.7925,0.5467,0.3096,0.2319,0.1736,0.121,0.0445,0.0539,0,2.6331,2.3181,2.0162,1.9176,1.8151,1.737,1.48,1.4316,1.3699
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,4.2,0.7292,0.5072,0.2908,0.2168,0.1627,0.1148,0.0371,0.0495,0,2.4887,2.2299,1.9491,1.8498,1.7439,1.667,1.4363,1.393,1.3358
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,4.4,0.6897,0.4769,0.2689,0.2046,0.1504,0.1091,0.0386,0.0523,0,2.3922,2.1471,1.8784,1.7859,1.6788,1.615,1.3975,1.3638,1.297
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,4.6,0.6366,0.4489,0.2652,0.2066,0.1516,0.1108,0.0406,0.0537,0,2.3247,2.1037,1.8812,1.7824,1.7007,1.6416,1.443,1.4066,1.3457
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,4.8,0.6262,0.4473,0.2599,0.1963,0.1472,0.1105,0.0429,0.0532,0,2.3056,2.0964,1.8734,1.8061,1.7164,1.6513,1.4448,1.4139,1.3577
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,5,0.6042,0.4352,0.2544,0.1945,0.1475,0.1041,0.0431,0.0532,0,2.2696,2.0709,1.8402,1.7584,1.6748,1.6179,1.4249,1.3868,1.3344
,2003-10-02T07:09:59Z,MD_4D,-16.43,145.5,5.2,0.6086,0.428,0.2542,0.1946,0.1452,0.1064,0.0394,0.0501,0,2.2423,2.0429,1.8173,1.7408,1.6565,1.5945,1.3992,1.3701,1.316
