GLOBAL ATTRIBUTES,,,,,,,,,,,,,,,,,,,,,,,
title,,,,,,,,,,,,,,,,,,,,,,,
project,Integrated Marine Observing System (IMOS),,,,,,,,,,,,,,,,,,,,,,
original_project_name,MERIS validation,,,,,,,,,,,,,,,,,,,,,,
institution,CSIRO Land and Water,,,,,,,,,,,,,,,,,,,,,,
abstract,This record contains in situ water optical property data collected in the Mossman-Daintree (Great Barrier Reef| Australia) on the RV Foundation One between 31 March-04 Apr 2004. A total of 8 stations were sampled during the field campaign MD04. The data can be used for Ocean Colour sensor validation| optical water quality monitoring and modelling. CLW deployed the optical instruments TriOS RAMSES (hyperspectral irradiance)| WETLabs AC-9 (absorption and attenuation coefficicents)| HOBILabs Hydroscat-6 (backscattering coefficient) during this cruise.This data was part of the dataset described in Oubelkheir et al. 2004| Bio-optical characterisation of Australian ocean| coastal and estuarine waters In SPIE| Ocean Optics XVII| Fremantle (Aus.) and Blondeau-Patissier et al. 2009| JGR| VOL. 114| C05003| doi:10.1029/2008JC005039. Funding for this study was provided by the Queensland Environmental Protection Agency| CSIRO Land and Water| CSIRO Marine and Atmospheric Research| and Coastal Catchments Initiative We also thank the captains and crews of the RV Foundation One.,,,,,,,,,,,,,,,,,,,,,,
conventions,IMOS biological data format (v1.0),,,,,,,,,,,,,,,,,,,,,,
date_created,,,,,,,,,,,,,,,,,,,,,,,
date_modified,,,,,,,,,,,,,,,,,,,,,,,
history,Data has been quality controlled by in-house procedures. AC-9 data were corrected for temperature using CTD profiles following Pegau et al.| 1997| Applied Optics| 36(24)| pp.6035-6046. The absorption coefficient was corrected for scattering using the proportional method described in Zaneveld et al.| 1992| Ocean Optics XI| vol.1750| pp.187-199| San Diego| CA,,,,,,,,,,,,,,,,,,,,,,
comment,The AC-9 measures the spectral absorption| A| and spectral attenuation| C| over nine wavelengths via a dual-path measurement. An AC-9 is used to measure the total absorption and attenuation of water (the signal includes both dissolved and particulate absorption). The AC-9 measurements were referenced to optically clean water| and resulting values thus represent the absorption and attenuation of the sample water minus the calibration water values. If the ocean water is very clear| it may be assumed that the measured absorption and attenuation represent everything in the water except pure water itself. To derive total absorption and total attenuation| it was therefore necessary to add in the absorption and attenuation| respectively| of pure water. Currently| the most accurate pure water absorption values are considered to be those presented by Pope and Fry (1997| Applied Optics| 36(33)| pp.8710-8723).,,,,,,,,,,,,,,,,,,,,,,
source,Total Absorption coefficients determined using an AC-9 (www.wetlabs.com),,,,,,,,,,,,,,,,,,,,,,
keywords,Oceans>Ocean Optics > Absorption ;Oceans>Ocean Optics>Ocean Colour;,,,,,,,,,,,,,,,,,,,,,,
cruise_id,Daintree_2004,,,,,,,,,,,,,,,,,,,,,,
vessel_name,RV Foundation One,,,,,,,,,,,,,,,,,,,,,,
platform_code,RVFO,,,,,,,,,,,,,,,,,,,,,,
references,http://www.marine.csiro.au/marq/edd_search.Browse_Citation?txtSession=9003,,,,,,,,,,,,,,,,,,,,,,
naming_authority,CSIRO Land and Water (CLW),,,,,,,,,,,,,,,,,,,,,,
cdm_data_type,profile,,,,,,,,,,,,,,,,,,,,,,
geospatial_lat_min,,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_lat_max,,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_lon_min,,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_lon_max,,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_vertical_min,0,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
geospatial_vertical_max,0,calculated automatically from table,,,,,,,,,,,,,,,,,,,,,
local_time_zone,10,AEST (UTC +10),,,,,,,,,,,,,,,,,,,,,
data_centre,CMAR data centre,,,,,,,,,,,,,,,,,,,,,,
data_centre_email,,,,,,,,,,,,,,,,,,,,,,,
author,"Brando, Vittorio",,,,,,,,,,,,,,,,,,,,,,
author_email,vittorio.brando@csiro.au,,,,,,,,,,,,,,,,,,,,,,
principal_investigator,"Steven, Andy",,,,,,,,,,,,,,,,,,,,,,
principal_investigator_email,Andy.Steven@csiro.au,,,,,,,,,,,,,,,,,,,,,,
publisher,Australian Ocean Data Network,,,,,,,,,,,,,,,,,,,,,,
publisher_email,info@aodn.org.au,,,,,,,,,,,,,,,,,,,,,,
institution,IMOS Satellite Remote Sensing - Ocean Colour Sub-facility - Bio-Optical Database of Australian Waters,,,,,,,,,,,,,,,,,,,,,,
institution_principal_investigator,"Schroeder, Thomas",,,,,,,,,,,,,,,,,,,,,,
institution_principal_investigator_email,Thomas.Schroeder@csiro.au,,,,,,,,,,,,,,,,,,,,,,
citation,"To cite this data set in a publication, use the format ""IMOS [year-of-data-downloaded], [Title], [data-access-url], accessed [date-of-access]""",,,,,,,,,,,,,,,,,,,,,,
acknowledgement,Data was sourced from the Commonwealth Scientific and Industrial Research Organisation (CSIRO),,,,,,,,,,,,,,,,,,,,,,
distribution_statement,Any users of IMOS data are required to clearly acknowledge the source of the material. Data, products and services from IMOS are provided as is without any warranty as to fitness for a particular purpose.,,,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
TABLE ROWS,,,,,,,,,,,,,,,,,,,,,,,
Row name,CF standard_name,IMOS long_name,Units,Fill value,Comments,,,,,,,,,,,,,,,,,,
Wavelength,radiation_wavelength,Wavelength of measuement,nm,-999,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
TABLE COLUMNS,,,,,,,,,,,,,,,,,,,,,,,
Column name,CF standard_name,IMOS long_name,Units,Fill value,Comments,,,,,,,,,,,,,,,,,,
Time,time,analysis_time,YYYY-MM-DDThh:mm:ssZ,,UTC date and time,,,,,,,,,,,,,,,,,,
Station_Code,,,,,National Reference Station code (6 letters),,,,,,,,,,,,,,,,,,
Latitude,latitude,latitude,degrees_north,-999,North positive,,,,,,,,,,,,,,,,,,
Longitude,longitude,longitude,degrees_east,-999,East positive,,,,,,,,,,,,,,,,,,
Depth,depth,depth,m,-999,positive down,,,,,,,,,,,,,,,,,,
ac9_a_corr,,corrected_volume_total_absorption_coefficient_in_sea_water,m-1,-999,,,,,,,,,,,,,,,,,,,
ac9_c_corr,,corrected_volume_total_attenuation_coefficient_in_sea_water,m-1,-999,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
,,,,,,,,,,,,,,,,,,,,,,,
DATA,,,,,,,,,,,,,,,,,,,,,,,
,Time,Station_Code,Latitude,Longitude,Depth,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_a_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr,ac9_c_corr
Wavelength,,,,,,412,440,488,510,532,555,650,676,715,412,440,488,510,532,555,650,676,715
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,0.2,0.6622,0.4328,0.2526,0.1915,0.1314,0.0936,0.028,0.0327,0,3.7792,3.5569,3.247,3.0996,2.9913,2.8837,2.5265,2.4512,2.336
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,0.4,0.6625,0.4411,0.2499,0.1899,0.1352,0.0968,0.0309,0.0322,0,3.7578,3.5324,3.217,3.0871,2.9758,2.8758,2.5131,2.4404,2.3143
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,0.6,0.6713,0.4368,0.2513,0.1916,0.1362,0.0978,0.0344,0.0357,0,3.8119,3.5946,3.2894,3.1243,3.0091,2.9189,2.56,2.489,2.3496
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,0.8,0.6622,0.4352,0.2451,0.1925,0.1338,0.0979,0.0334,0.0359,0,3.7946,3.5616,3.2406,3.1157,2.9971,2.8858,2.5318,2.4573,2.3259
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,1,0.6647,0.4432,0.2429,0.1863,0.1361,0.0897,0.0321,0.0336,0,3.8193,3.5745,3.2444,3.1358,3.0048,2.9094,2.5493,2.4396,2.3429
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,1.2,0.6757,0.4564,0.2603,0.2025,0.1487,0.1146,0.0437,0.0414,0,3.8267,3.6597,3.286,3.1504,3.0583,2.9292,2.5719,2.4859,2.4029
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,1.4,0.6658,0.4474,0.2614,0.1914,0.1328,0.0974,0.0338,0.037,0,3.8373,3.6106,3.2767,3.1593,3.0541,2.9466,2.577,2.5003,2.3838
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,1.6,0.6695,0.4583,0.2656,0.2048,0.1419,0.1015,0.0344,0.0391,0,3.8158,3.5884,3.2607,3.1367,3.0196,2.92,2.5504,2.4888,2.357
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,1.8,0.6709,0.4529,0.2605,0.1925,0.1459,0.0986,0.028,0.0339,0,3.8078,3.6083,3.2619,3.1549,3.0255,2.9377,2.562,2.4978,2.3672
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,2,0.675,0.4507,0.2628,0.1977,0.139,0.1093,0.0377,0.0384,0,3.8062,3.5552,3.2309,3.1355,3.0235,2.9181,2.5216,2.479,2.3764
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,2.2,0.6649,0.4463,0.2602,0.1987,0.1388,0.0967,0.0334,0.0304,0,3.7882,3.5312,3.2216,3.1236,2.9839,2.9013,2.5397,2.4694,2.3554
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,2.4,0.6723,0.4415,0.2532,0.1885,0.1399,0.1074,0.0298,0.0352,0,3.7228,3.5116,3.2029,3.0868,2.9722,2.8511,2.5122,2.417,2.3174
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,2.6,0.6712,0.441,0.2552,0.185,0.1287,0.0947,0.0343,0.0337,0,3.7367,3.4982,3.194,3.0528,2.9632,2.8434,2.4894,2.4146,2.3062
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,2.8,0.6521,0.4282,0.252,0.1891,0.1369,0.096,0.0373,0.0346,0,3.6938,3.4636,3.1372,3.0496,2.9257,2.8251,2.4757,2.4182,2.3134
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,3,0.6234,0.4072,0.2456,0.1741,0.1229,0.0936,0.0304,0.0373,0,3.4332,3.1682,2.8983,2.7675,2.6747,2.5375,2.1891,2.1515,2.076
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,3.2,0.6163,0.412,0.2423,0.1654,0.1187,0.0694,0.0332,0.0365,0,3.4627,3.2569,2.958,2.8949,2.7803,2.6881,2.3407,2.2549,2.1311
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,3.4,0.5476,0.3636,0.2085,0.1596,0.1044,0.0773,0.0248,0.0319,0,2.6155,2.452,2.208,2.1135,2.0586,1.9546,1.7335,1.6717,1.5925
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,3.6,0.546,0.3617,0.1992,0.1576,0.1165,0.086,0.0371,0.0333,0,2.5725,2.4226,2.1778,2.0927,1.9891,1.9215,1.685,1.6345,1.5564
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,3.8,0.534,0.3486,0.1998,0.151,0.1047,0.076,0.0257,0.0291,0,2.3282,2.1733,2.0335,1.8888,1.8041,1.7334,1.5132,1.4939,1.4012
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,4,0.5421,0.3538,0.1984,0.152,0.1029,0.0795,0.0244,0.0263,0,2.6452,2.426,2.1982,2.0945,2.0183,1.9369,1.7068,1.6652,1.5698
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,4.2,0.5101,0.3459,0.1882,0.1492,0.1094,0.0871,0.0254,0.0293,0,2.429,2.2689,2.0271,1.9802,1.906,1.8139,1.5942,1.5397,1.4667
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,4.4,0.5306,0.3481,0.1966,0.155,0.1115,0.0776,0.0257,0.0269,0,2.3796,2.1951,1.9831,1.9242,1.8459,1.7709,1.56,1.5153,1.4508
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,4.6,0.4819,0.3109,0.176,0.1337,0.0929,0.0665,0.0259,0.0228,0,2.001,1.8627,1.6702,1.6037,1.5588,1.4935,1.3086,1.2872,1.2126
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,4.8,0.4935,0.3233,0.1892,0.1454,0.0994,0.0672,0.0222,0.0214,0,2.1096,1.9571,1.7723,1.6968,1.6228,1.574,1.3664,1.306,1.254
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,5,0.4994,0.3353,0.1871,0.1408,0.1012,0.0837,0.0276,0.0201,0,2.0665,1.9251,1.7406,1.6702,1.6159,1.5405,1.3481,1.3384,1.2434
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,5.2,0.4534,0.3076,0.1688,0.1233,0.0908,0.0694,0.0208,0.0261,0,1.7635,1.5854,1.4695,1.3615,1.319,1.28,1.1226,1.0765,1.0283
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,5.4,0.4551,0.2926,0.1747,0.1229,0.084,0.0638,0.0252,0.0253,0,1.714,1.5941,1.4081,1.3668,1.3096,1.249,1.0947,1.0765,1.0299
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,5.6,0.4619,0.2955,0.1741,0.1378,0.0955,0.0691,0.0268,0.0222,0,1.7791,1.6595,1.4717,1.4082,1.3843,1.2977,1.1661,1.1204,1.0811
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,5.8,0.4675,0.315,0.1873,0.1366,0.0939,0.0743,0.0305,0.0233,0,1.7735,1.636,1.457,1.4046,1.3425,1.2912,1.1323,1.127,1.0427
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,6,0.468,0.3022,0.1731,0.127,0.0912,0.0551,0.0233,0.0197,0,1.7951,1.6706,1.4937,1.449,1.3847,1.3475,1.1835,1.15,1.1006
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,6.2,0.4517,0.2986,0.1738,0.1271,0.0861,0.0504,0.0228,0.0136,0,1.7811,1.6772,1.4821,1.4268,1.3633,1.3231,1.1684,1.1476,1.0804
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,6.4,0.4616,0.3069,0.1748,0.1347,0.094,0.0702,0.0234,0.0221,0,1.8743,1.7671,1.5971,1.5528,1.4398,1.3975,1.2371,1.2082,1.1617
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,6.6,0.4608,0.307,0.1706,0.1305,0.0932,0.0632,0.0297,0.0231,0,1.8596,1.6963,1.5443,1.4769,1.4264,1.3845,1.2156,1.1897,1.142
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,6.8,0.4668,0.3112,0.1732,0.1364,0.0957,0.0594,0.022,0.0202,0,1.8425,1.711,1.5322,1.4894,1.4368,1.3745,1.2291,1.1824,1.13
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,7,0.4635,0.3068,0.1745,0.1373,0.0935,0.0715,0.0259,0.0194,0,1.8794,1.7435,1.5862,1.5203,1.4468,1.4003,1.2403,1.2008,1.1352
,2004-03-31T00:00:00Z,MD_5W,-16.2,145.48,7.2,0.4743,0.3069,0.177,0.1337,0.1006,0.0685,0.0236,0.0232,0,1.9141,1.7758,1.6096,1.5396,1.4816,1.4381,1.2659,1.2338,1.1658
