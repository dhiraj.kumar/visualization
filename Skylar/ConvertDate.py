import datetime


def convertDate_Full_To_HMS(d):
    date_time_convert = datetime.datetime.strptime(d, "%Y-%m-%dT%H:%M:%SZ")
    new_format = "%H:%M:%S"
    result = date_time_convert.strftime(new_format)
    return result


def convertDate_Full_To_YMD(d):
    date_time_convert = datetime.datetime.strptime(d, "%Y-%m-%dT%H:%M:%SZ")
    new_format = "%Y-%m-%d"
    result = date_time_convert.strftime(new_format)
    return result


def convertDate_Full_To_YMDHMS(d):
    date_time_convert = datetime.datetime.strptime(d, "%Y-%m-%dT%H:%M:%SZ")
    new_format = "%Y-%m-%dT%H:%M:%SZ"
    result = date_time_convert.strftime(new_format)
    return result


def test(d):
    date_time_convert = datetime.datetime.strptime(d, "%Y-%m-%dT%H:%M:%SZ")
    # print(type(date_time_convert))
    new_format = "%d"
    result = date_time_convert.strftime(new_format)
    # print(type(result))
    return result
