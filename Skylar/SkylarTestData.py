import numpy as np
import pandas as pd
import xarray as xr

lon = 30
lat = 10

data = xr.open_dataset("data\complete\ct5km_hs_v3.1_19860101.nc")
print("ONE")
print(type(data))

print("TWO")
print(data)

print("THREE")
print(data.keys)

print("FOUR")
ds = data.hotspot.sel(
    time="1986-01-01T12:00:00", lat=44.875, lon=-128.975, method="nearest"
)
print(ds)

print("FIVE")
print(data["hotspot"])

print("SIX")
print(data.lat)

print("SEVEN")
print(data.lat.coords)

print("EIGHT")
print(data.lat)
print()
print(data.coords)
print()
print(data.data_vars)
print()
print(data.hotspot)
print("HEY")
print(
    data.hotspot.sel(
        time="1986-01-01T12:00:00", lat=44.875, lon=-128.975, method="nearest"
    )
)
print("HIGH")
print(
    data.hotspot.sel(
        time="1986-01-01T12:00:00", lat=44.875, lon=-128.975, method="nearest"
    ).data
)

# import h5py

# f = h5py.File("Q2011_09_RFI.h5", "r")

# print(list(f.keys()))

# dset = f["Radiometer"]

# print(list(dset.keys()))

# print(dset["Rad_RFI_percent_Asc"])

# from dbfread import DBF
# import sys
# import csv

# # for record in DBF("dugong_finescale.dbf"):
# #     print(record)

# # dbf_record = DBF("dugong_finescale.dbf")

# # print(type(dbf_record))
# # print(dbf_record)
# # print(dbf_record.field_names)

# # writer = csv.writer(sys.stdout)

# # writer.writerow(dbf_record.field_names)

# # i = 0
# # for record in dbf_record:
# #     if i <= 5:
# #         writer.writerow(list(record.values()))
# #         print()
# #         i += 1
# #     else:
# #         break

# #### Idk what this data represents, thus ignoring for now
