# import

import pandas as pd

# Read data

df = pd.read_csv(r"./data/complete/GBR/NoHead-All.csv")

# Clean data

print("Original Shape:")
print(df.shape)

print("Depth > 0:")
df = df[df["DEPTH"] > 0]
print(df.shape)
print("Depth notna:")
df = df[df["DEPTH"].notna()]

print("IRRAD443 > 0:")
df = df[df["IRRAD443"] > 0]
print(df.shape)
print("IRRAD443 notna:")
df = df[df["IRRAD443"].notna()]
print(df.shape)

print("IRRAD490 > 0:")
df = df[df["IRRAD490"] > 0]
print(df.shape)
print("IRRAD490 notna:")
df = df[df["IRRAD490"].notna()]
print(df.shape)

print("IRRAD555 > 0:")
df = df[df["IRRAD555"] > 0]
print(df.shape)
print("IRRAD555 notna:")
df = df[df["IRRAD555"].notna()]
print(df.shape)

print("IRRAD670 > 0:")
df = df[df["IRRAD670"] > 0]
print(df.shape)
print("IRRAD670 notna:")
df = df[df["IRRAD670"].notna()]
print(df.shape)

print("Time notna:")
df = df[df["TIME"].notna()]
print(df.shape)

print("CNDC notna:")
df = df[df["CNDC"].notna()]
print(df.shape)

print("PSAL notna:")
df = df[df["PSAL"].notna()]
print(df.shape)

print("VBSC notna:")
df = df[df["VBSC"].notna()]
print(df.shape)

# Only take Slocum glider

print("Slocum only:")
df = df[(df.platform_type == "slocum glider")].sort_values(by=["TIME"])
print(df.shape)

# Drop non-needed columns

df = df.drop(
    [
        "deployment_name",
        "platform_type",
        "platform_code",
        "time_coverage_start",
        "time_coverage_end",
        "TIME_quality_control",
        "LATITUDE",
        "LATITUDE_quality_control",
        "LONGITUDE",
        "LONGITUDE_quality_control",
        "PRES",
        "PRES_quality_control",
        "DEPTH_quality_control",
        "PROFILE",
        "PROFILE_quality_control",
        "PHASE",
        "PHASE_quality_control",
        "TEMP",
        "TEMP_quality_control",
        "PSAL_quality_control",
        "DOX1_quality_control",
        "DOX2",
        "DOX2_quality_control",
        "CPHL_quality_control",
        "CDOM_quality_control",
        "CNDC_quality_control",
        "VBSC_quality_control",
        "VBSC_quality_control",
        "NTRA",
        "NTRA_quality_control",
        "UCUR",
        "UCUR_quality_control",
        "VCUR",
        "VCUR_quality_control",
        "HEAD",
        "HEAD_quality_control",
        "UCUR_GPS",
        "UCUR_GPS_quality_control",
        "VCUR_GPS",
        "VCUR_GPS_quality_control",
        "IRRAD443_quality_control",
        "IRRAD490_quality_control",
        "IRRAD555_quality_control",
        "IRRAD670_quality_control",
        "geom",
    ],
    axis=1,
)

# Output to CSV

df.to_csv(r"./data/complete/GBR/NoHead-AllCleaned.csv")
