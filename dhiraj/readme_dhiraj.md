# Visualisation
- The idea of the project (if possible) to find factors affecting the bleaching of the Coral reefs in the Great Barrier Region (Australia).

### Data Source
- All the data is taken from [here](https://portal.aodn.org.au/) and the [data catalogue](https://catalogue-imos.aodn.org.au/geonetwork/srv/eng/catalog.search#/metadata/006bb7dc-860b-4b89-bf4c-6bd930bd35b7)
- All data was cleaned and saved in the folder `clean`.

### Files
- `dataCleaner.ipynb` contains the code to download the entire data. Data source (above) had a way to download the data using python scripts. It also contains the code to clean the data and save it different files for further analysis. It also codes of the initial cleaning of the data and analysis.
- `final.ipynb` contains all the plots, as well as all the helper functions.

### Analysis
- Although there was a lot of data available, all was not useful. Therefore, I decided to focus on the time period of 2019 to 2022. Why? Because I was able to find some interesting trends and facts that could be [corroborated with real sources](https://www.aims.gov.au/research-topics/environmental-issues/coral-bleaching/coral-bleaching-events). The sources are provided in the final presentation as well.
- I, primarily, looked into in the physical properties that might help us in identifying the factors responsible for bleaching of the Coral reefs in the Great Barrier Region. I looked into physical properties like Air Temperature, Air Pressure, Wind Speed, Percentage of Relative Humidity, etc. However, I could find useful data only for Air Temperature and Percentage of Relative Humidity and that's what I focused on.

### Result
- I was successfully able to find some factors that had some affect on the overall health of Coral reefs and that were directly contributing to the bleaching.
